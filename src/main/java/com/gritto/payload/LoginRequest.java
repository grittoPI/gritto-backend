package com.gritto.payload;

import javax.validation.constraints.NotBlank;

public class LoginRequest {

    @NotBlank(message = "O nome de usuario nao pode ficar em branco")
    private String username;
    @NotBlank(message = "A senha nao pode ficar em branco")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return username + " - " + password;
    }
}
