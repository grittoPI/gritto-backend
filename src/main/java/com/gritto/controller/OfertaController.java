package com.gritto.controller;

import com.gritto.service.OfertaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OfertaController {

    private final OfertaService service;
}
