package com.gritto.controller;

import com.gritto.service.PublicacaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PublicacaoController {

    private final PublicacaoService service;
}
