package com.gritto.controller;

import com.gritto.service.RuaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RuaController {

    private final RuaService service;
}
