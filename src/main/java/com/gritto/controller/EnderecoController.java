package com.gritto.controller;

import com.gritto.service.EnderecoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class EnderecoController {

    private final EnderecoService service;
}