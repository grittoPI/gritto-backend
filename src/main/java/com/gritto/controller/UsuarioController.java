package com.gritto.controller;

import com.gritto.model.CategoriaServico;
import com.gritto.model.Usuario;
import com.gritto.payload.JWTLoginSucessReponse;
import com.gritto.payload.LoginRequest;
import com.gritto.security.JwtTokenProvider;
import com.gritto.service.CategoriaServicoService;
import com.gritto.service.MapValidationErrorService;
import com.gritto.service.UsuarioService;
import com.gritto.validator.UsuarioValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.gritto.security.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/usuarios")
public class UsuarioController {

    private final UsuarioService usuarioService;

    private final CategoriaServicoService categoriaServicoService;

    private final UsuarioValidator usuarioValidator;

    private final MapValidationErrorService mapValidationErrorService;

    private final JwtTokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    @GetMapping("/usuarios")
    public List<Usuario> getAllUsuarios() {
        return usuarioService.findAll();
    }

    @GetMapping("/usuarios/{id}")
    public Optional<Usuario> getUserById(@PathVariable Long id) {

        return usuarioService.findById(id);
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult result){
        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if(errorMap != null) return errorMap;

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = TOKEN_PREFIX +  tokenProvider.generateToken(authentication);

        return ResponseEntity.ok(new JWTLoginSucessReponse(true, jwt));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUsuario(@Valid @RequestBody Usuario usuario, BindingResult result){
        // Validate passwords match
        usuarioValidator.validate(usuario,result);

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if(errorMap != null)return errorMap;

        Usuario newUsuario = usuarioService.saveUsuario(usuario);

        return  new ResponseEntity<Usuario>(newUsuario, HttpStatus.CREATED);
    }

    @GetMapping("/categoriaservico/{id}")
    public ResponseEntity getChannelsByRecloser(@PathVariable long id) {

        CategoriaServico categoriaServico = categoriaServicoService.getById(id);

        List<Usuario> list = usuarioService.getUsuarioByCategoriaServico(categoriaServico);

        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, null, HttpStatus.OK);
        }
        return new ResponseEntity<>(list, null, HttpStatus.NO_CONTENT);
    }
}