package com.gritto.repository;

import com.gritto.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    List<Usuario>findByCategoriaServico(Long id);

    @Query("select u from Usuario u where u.username like :username")
    Usuario findByUsername(@Param("username") String username);

    //Usuario findByUsername(String username);
    Usuario getById(Long id);

}