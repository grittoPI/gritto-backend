package com.gritto.service;

import com.gritto.datatables.Datatables;
import com.gritto.datatables.DatatablesColunas;
import com.gritto.model.CategoriaServico;
import com.gritto.repository.CategoriaServicoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CategoriaServicoService {

    private final CategoriaServicoRepository repository;

    private final Datatables datatables;

    @Transactional(readOnly = false)
    public CategoriaServico adicionarCategoriaServico(CategoriaServico categoriaServico) {

       return repository.save(categoriaServico);
    }

    @Transactional(readOnly = true)
    public Map<String, Object> buscarCategoriasServicos(HttpServletRequest request) {
        datatables.setRequest(request);
        datatables.setColunas(DatatablesColunas.CATEGORIASERICO);
        Page<?> page = datatables.getSearch().isEmpty()
                ? repository.findAll(datatables.getPageable())
                : repository.findAllByNome(datatables.getSearch(), datatables.getPageable());
        return datatables.getResponse(page);
    }

    public CategoriaServico getById(long id) {
        return repository.getOne(id);
    }

}
