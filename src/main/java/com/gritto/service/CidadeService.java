package com.gritto.service;

import com.gritto.repository.CidadeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CidadeService {

    private final CidadeRepository repository;
}