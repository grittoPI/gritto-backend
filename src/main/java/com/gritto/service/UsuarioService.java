package com.gritto.service;

import com.gritto.execptions.UsernameAlreadyExistsException;
import com.gritto.model.CategoriaServico;
import com.gritto.model.Usuario;
import com.gritto.repository.UsuarioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UsuarioService {

    private final UsuarioRepository usuarioRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public Usuario saveUsuario (Usuario newUsuario){

        try{
            newUsuario.setPassword(bCryptPasswordEncoder.encode(newUsuario.getPassword()));

            newUsuario.setUsername(newUsuario.getUsername());
            newUsuario.setConfirmPassword("");

            return usuarioRepository.save(newUsuario);

        }catch (Exception e){
            throw new UsernameAlreadyExistsException("Nome '"+newUsuario.getUsername()+"' already exists");
        }

    }

    public List<Usuario> findAll() {

        return usuarioRepository.findAll();
    }

    public Optional<Usuario> findById(Long id) {

        return usuarioRepository.findById(id);
    }

    public List<Usuario>getUsuarioByCategoriaServico(Long id) {

        return usuarioRepository.findByCategoriaServico(id);
    }

    public List<Usuario> getUsuarioByCategoriaServico(CategoriaServico categoriaServico) {

        List<Usuario> usuarios = usuarioRepository.findAll();

        return usuarios.stream()
                .filter(usuario -> usuario.getCategoriaServico().equals(categoriaServico))
                .collect(Collectors.toList());
    }

}
