package com.gritto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.*;

@Entity
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Usuario implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String telefone;

    @Email(message = "Nome de usuario deve ser um e-mail")
    @NotBlank(message = "Nome do usuario e obrigatorio!")
    @Column(unique = true)
    private String username;

    @NotBlank(message = "Nome completo")
    private String fullName;

    @NotBlank(message = "O campo da senha e obrigatório!")
    private String password;

    @Transient
    private String confirmPassword;
    private Date create_At;
    private Date update_At;

    @JoinColumn(name = "id_categoriaServico")
    @ManyToOne
    private CategoriaServico categoriaServico;

    @ManyToMany
    @JoinTable(
            name = "usuarios_tem_perfis",
            joinColumns = { @JoinColumn(name = "usuario_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "perfil_id", referencedColumnName = "id") }
    )

    @JsonIgnore
    private List<Perfil> perfis;

    @JsonIgnore
    @Column(name = "ativo", nullable = false, columnDefinition = "TINYINT(1)")
    private boolean ativo;

    @JsonIgnore
    @Column(name = "codigo_verificador", length = 6)
    private String codigoVerificador;

    public Usuario() {
        super();
    }


    // adiciona perfis a lista
    public void addPerfil(PerfilTipo tipo) {
        if (this.perfis == null) {
            this.perfis = new ArrayList<>();
        }
        this.perfis.add(new Perfil(tipo.getCod()));
    }

    public List<Perfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(List<Perfil> perfis) {
        this.perfis = perfis;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @PrePersist
    protected void onCreate(){
        this.create_At = new Date();
    }

    @PreUpdate
    protected void onUpdate(){
        this.update_At = new Date();
    }

    /*
    UserDetails interface methods
     */

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.<GrantedAuthority>singletonList(new SimpleGrantedAuthority("User"));
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }
}